﻿namespace FizzBuzz.Web.Tests
{
    using System.Web.Mvc;
    using FluentAssertions;
    using Controllers;
    using Business;
    using Models;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using NUnit.Framework;
    using System.Linq;
    using System.Collections.Generic;
    using System.Globalization;

    [TestClass]
    public class FizzBuzzControllerUnitTest
    {

        private Mock<IFizzBuzzManager> fizzBuzzManager;
        private FizzBuzzController controller;

        [SetUp]
        public void TestInitialize()
        {
            this.fizzBuzzManager = new Mock<IFizzBuzzManager>();
            this.controller = new FizzBuzzController(this.fizzBuzzManager.Object);
        }

        [TestCase]
        public void FizzBuzzReturnsValidView()
        {
            var fizzbuzzController = new FizzBuzzController(null);

            var result = fizzbuzzController.FizzBuzz();

            var viewResult = result as ViewResult;
            viewResult.Should().NotBeNull("Should return an ActionResult");
            viewResult.ViewName.ShouldBeEquivalentTo("FizzBuzzView");
        }

        [TestCase]
        public void FizzBuzzReturnsValidFizzBuzzSequenceForGivenNumber()
        {
            var inputFizzBuzzNumber = 20;
            this.fizzBuzzManager.Setup(m => m.GetFizzBuzzList(inputFizzBuzzNumber)).Returns(GetFizzBuzzList(inputFizzBuzzNumber));

            var result = this.controller.FizzBuzz(new FizzBuzzModel { InputValue = inputFizzBuzzNumber });

            var viewResult = result as ViewResult;
            viewResult.Should().NotBeNull("Should return an ActionResult");
            var viewModel = viewResult.Model as FizzBuzzModel;
            viewModel.Should().NotBeNull();
            viewModel.FizzBuzzValuesList.Count().ShouldBeEquivalentTo(inputFizzBuzzNumber);
            viewModel.FizzBuzzValuesList.ElementAt(0).ShouldBeEquivalentTo("1");
            viewModel.FizzBuzzValuesList.ElementAt(19).ShouldBeEquivalentTo("20");
        }

        [TestCase(21, 0, false, true)]
        [TestCase(65, 2, true, true)]
        [TestCase(25, 1, true, false)]
        public void DisplayPagingReturnsNextLinkForGivenNumber(int inputFizzBuzzNumber, int pageIndex, bool prevLinkValue, bool nextLinkValue)
        {
            this.fizzBuzzManager.Setup(m => m.GetFizzBuzzList(inputFizzBuzzNumber)).Returns(GetFizzBuzzList(inputFizzBuzzNumber));

            var result = this.controller.DisplayPaging(inputFizzBuzzNumber, pageIndex, "Next");

            var viewResult = result as ViewResult;
            viewResult.Should().NotBeNull("Should return an ActionResult");
            var viewModel = viewResult.Model as FizzBuzzModel;
            viewModel.Should().NotBeNull();
            viewModel.ShowNextLink.ShouldBeEquivalentTo(nextLinkValue);
            viewModel.ShowPrevLink.ShouldBeEquivalentTo(prevLinkValue);
        }

        [TestCase(21, 0, 20)]
        [TestCase(65, 2, 20)]
        [TestCase(25, 1, 5)]
        [TestCase(5, 0, 5)]
        [TestCase(20, 0, 20)]
        public void DisplayPagingReturnsPageCountForGivenNumber(int inputFizzBuzzNumber, int pageIndex, int pageCount)
        {
            this.fizzBuzzManager.Setup(m => m.GetFizzBuzzList(inputFizzBuzzNumber)).Returns(GetFizzBuzzList(inputFizzBuzzNumber));

            var result = this.controller.DisplayPaging(inputFizzBuzzNumber, pageIndex, "Next");

            var viewResult = result as ViewResult;
            viewResult.Should().NotBeNull("Should return an ActionResult");
            var viewModel = viewResult.Model as FizzBuzzModel;
            viewModel.Should().NotBeNull();
            viewModel.FizzBuzzValuesList.Count().ShouldBeEquivalentTo(pageCount);
        }


        private IEnumerable<string> GetFizzBuzzList(int rowsToBeTested)
        {
            var fizzbuzzlist = Enumerable.Range(1, rowsToBeTested).Select(i => i.ToString(CultureInfo.InvariantCulture));
            return fizzbuzzlist;
        }
    }
}
