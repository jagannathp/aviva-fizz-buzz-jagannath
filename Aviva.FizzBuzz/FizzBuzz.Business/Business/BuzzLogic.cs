﻿namespace FizzBuzz.Business
{
    using Resources;

    public class BuzzLogic : IDivisionPolicy
    {
        public int Order { get; set; }

        public bool IsNumberDivisible(int inputFizzBuzzNumber)
        {
            return inputFizzBuzzNumber % 5 == 0;
        }

        public string GetDisplayMessage(bool isTodayWednesday)
        {
            return (isTodayWednesday ? BusinessResources.Wuzz : BusinessResources.Buzz);
        }

    }
}
