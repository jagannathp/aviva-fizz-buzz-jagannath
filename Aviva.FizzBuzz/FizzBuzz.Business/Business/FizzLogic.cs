﻿namespace FizzBuzz.Business
{
    using Resources;

    public class FizzLogic : IDivisionPolicy
    {
        public int Order { get; set; }

        public bool IsNumberDivisible(int inputFizzBuzzNumber)
        {
            return inputFizzBuzzNumber % 3 == 0;
        }

        public string GetDisplayMessage(bool isTodayWednesday)
        {
            return isTodayWednesday ? BusinessResources.Wizz : BusinessResources.Fizz;
        }

    }
}
