﻿namespace FizzBuzz.Business
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Globalization;

    public class FizzBuzzManager : IFizzBuzzManager
    {
        private readonly IEnumerable<IDivisionPolicy> divisionPolicy;
        private readonly IDayProvider dayProvider;

        public FizzBuzzManager(IEnumerable<IDivisionPolicy> divisionPolicy, IDayProvider dayProvider)
        {
            this.divisionPolicy = divisionPolicy.OrderBy(x => x.Order);
            this.dayProvider = dayProvider;
        }

        public IEnumerable<string> GetFizzBuzzList(int inputValue)
        {
            var fizzbuzzResultList = new List<string>();
            for (var counter = 1; counter <= inputValue; counter++)
            {
                var divionPolicies = this.divisionPolicy.Where(x => x.IsNumberDivisible(counter));
                var fizzBuzzItem = divionPolicies.FirstOrDefault();
                fizzbuzzResultList.Add(fizzBuzzItem != null
                    ? fizzBuzzItem.GetDisplayMessage(this.dayProvider.IsTodayWednesday())
                    : counter.ToString(CultureInfo.InvariantCulture));
            }
            return fizzbuzzResultList;
        }
    }
}
