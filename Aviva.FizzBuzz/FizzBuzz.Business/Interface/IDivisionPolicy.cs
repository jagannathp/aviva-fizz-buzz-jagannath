﻿namespace FizzBuzz.Business
{
    public interface IDivisionPolicy
    {
        int Order { get; set; }

        bool IsNumberDivisible(int number);

        string GetDisplayMessage(bool isTodayWednesday);
    }
}
