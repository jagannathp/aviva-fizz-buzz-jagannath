﻿namespace FizzBuzz.Business.Tests
{
    using FluentAssertions;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using NUnit.Framework;
    using Moq;
    using System.Collections.Generic;
    using Resources;
    using System.Linq;

    [TestClass]
    public class FizzBuzzManagerUnitTest
    {
        private Mock<IDayProvider> dayProvider;
        private IEnumerable<IDivisionPolicy> divisionPolicies;

        [SetUp]
        public void TestInitialize()
        {
            this.dayProvider = new Mock<IDayProvider>();
            this.divisionPolicies = new List<IDivisionPolicy>()
            {
                GetFizzLogicMockData().Object,
                GetBuzzLogicMockData().Object,
                GetFizzBuzzLogicMockData().Object
            };
        }

        [TestCase]
        public void GetFizzBuzzListReturnsSequenceOfNumberWithFizzBuzzForGivenInputNonWednesDay()
        {
            this.dayProvider.Setup(x => x.IsTodayWednesday()).Returns(false);
            var fizzbuzzLogic = new FizzBuzzManager(this.divisionPolicies, this.dayProvider.Object);

            var resultList = fizzbuzzLogic.GetFizzBuzzList(15);

            resultList.Should().NotBeNullOrEmpty();
            resultList.Count().Should().Be(15);
            resultList.ElementAt(2).Should().Be(BusinessResources.Fizz);
            resultList.ElementAt(4).Should().Be(BusinessResources.Buzz);
            resultList.ElementAt(14).Should().Be(BusinessResources.FizzBuzz);
        }


        [TestCase]
        public void GetFizzBuzzListReturnsSequenceOfNumberWithWizzWuzzForGivenInputWednesDay()
        {
            this.dayProvider.Setup(x => x.IsTodayWednesday()).Returns(true);
            var fizzbuzzLogic = new FizzBuzzManager(this.divisionPolicies, this.dayProvider.Object);

            var resultList = fizzbuzzLogic.GetFizzBuzzList(30);

            resultList.Should().NotBeNullOrEmpty();
            resultList.Count().Should().Be(30);
            resultList.ElementAt(2).Should().Be(BusinessResources.Wizz);
            resultList.ElementAt(4).Should().Be(BusinessResources.Wuzz);
            resultList.ElementAt(14).Should().Be(BusinessResources.WizzWuzz);
        }

        private static Mock<IDivisionPolicy> GetFizzLogicMockData()
        {
            var divisionPolicy = new Mock<IDivisionPolicy>();
            divisionPolicy.Setup(x => x.Order).Returns(3);
            divisionPolicy.Setup(x => x.IsNumberDivisible(It.IsAny<int>())).Returns(false);
            divisionPolicy.Setup(x => x.IsNumberDivisible(3)).Returns(true);
            divisionPolicy.Setup(x => x.IsNumberDivisible(6)).Returns(true);
            divisionPolicy.Setup(x => x.IsNumberDivisible(9)).Returns(true);
            divisionPolicy.Setup(x => x.IsNumberDivisible(12)).Returns(true);
            divisionPolicy.Setup(x => x.IsNumberDivisible(15)).Returns(true);
            divisionPolicy.Setup(x => x.GetDisplayMessage(false)).Returns(BusinessResources.Fizz);
            divisionPolicy.Setup(x => x.GetDisplayMessage(true)).Returns(BusinessResources.Wizz);
            return divisionPolicy;
        }

        private static Mock<IDivisionPolicy> GetBuzzLogicMockData()
        {
            var divisionPolicy = new Mock<IDivisionPolicy>();
            divisionPolicy.Setup(x => x.Order).Returns(2);
            divisionPolicy.Setup(x => x.IsNumberDivisible(It.IsAny<int>())).Returns(false);
            divisionPolicy.Setup(x => x.IsNumberDivisible(5)).Returns(true);
            divisionPolicy.Setup(x => x.IsNumberDivisible(10)).Returns(true);
            divisionPolicy.Setup(x => x.IsNumberDivisible(15)).Returns(true);
            divisionPolicy.Setup(x => x.GetDisplayMessage(false)).Returns(BusinessResources.Buzz);
            divisionPolicy.Setup(x => x.GetDisplayMessage(true)).Returns(BusinessResources.Wuzz);
            return divisionPolicy;
        }

        private static Mock<IDivisionPolicy> GetFizzBuzzLogicMockData()
        {
            var divisionPolicy = new Mock<IDivisionPolicy>();
            divisionPolicy.Setup(x => x.Order).Returns(1);
            divisionPolicy.Setup(x => x.IsNumberDivisible(It.IsAny<int>())).Returns(false);
            divisionPolicy.Setup(x => x.IsNumberDivisible(15)).Returns(true);
            divisionPolicy.Setup(x => x.IsNumberDivisible(30)).Returns(true);
            divisionPolicy.Setup(x => x.GetDisplayMessage(false)).Returns(BusinessResources.FizzBuzz);
            divisionPolicy.Setup(x => x.GetDisplayMessage(true)).Returns(BusinessResources.WizzWuzz);
            return divisionPolicy;
        }

    }
}
