namespace FizzBuzz.Web
{

    using StructureMap;
    using System.Web.Mvc;
    using Business;

    public static class IoC
    {
        public static IContainer Container
        {
            get
            {
                var container = new Container(x =>
                {
                    x.For<IFizzBuzzManager>().Use<FizzBuzzManager>();
                    x.For<IDivisionPolicy>().Use<FizzLogic>().SetProperty(a => a.Order = 2);
                    x.For<IDivisionPolicy>().Use<BuzzLogic>().SetProperty(a => a.Order = 3);
                    x.For<IDivisionPolicy>().Use<FizzBuzzLogic>().SetProperty(a => a.Order = 1);
                    x.For<IDayProvider>().Use<DayProvider>();
                });

                DependencyResolver.SetResolver(new SmDependencyResolver(container));
                return container;
            }
        }
    }
}