[assembly: WebActivator.PreApplicationStartMethod(typeof(FizzBuzz.Web.App_Start.StructuremapMvc), "Start")]

namespace FizzBuzz.Web.App_Start
{
    public static class StructuremapMvc
    {
        public static void Start()
        {
            IoC.Container.GetInstance<SmDependencyResolver>();
        }
    }
}