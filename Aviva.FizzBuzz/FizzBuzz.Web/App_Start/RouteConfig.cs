﻿namespace FizzBuzz.Web
{
    using System.Web.Mvc;
    using System.Web.Routing;

    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
              name: "Input",
              url: "{controller}/{action}/{id}",
              defaults: new { controller = "FizzBuzz", action = "FizzBuzz", id = UrlParameter.Optional }
          );

            routes.MapRoute(
               name: "DisplayList",
               url: "{controller}/{action}/{id}",
               defaults: new { controller = "FizzBuzz", action = "FizzBuzz", id = UrlParameter.Optional }
           );

            routes.MapRoute(
                name: "ShowNextPrev",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "FizzBuzz", action = "DisplayPaging", id = UrlParameter.Optional }
            );
        }
    }
}